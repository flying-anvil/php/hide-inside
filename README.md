# Hide-Inside

Hide arbitrary data inside of images.

Currently, only png files are supported. If the image contains transparency, it will be lost.
In the future, reading any format and writing also bmp might be possible.

## Usage

It's recommended to compress the file before you hide it.

Hide data in an image:

`hide-inside hide data/sample.txt data/Blank_32.png data/hidden.png`

---

Reveal hidden data:

`hide-inside reveal data/hidden.png data/revealed`

---

Check if image revealable data:

`hide-inside check data/hidden.png`

### Utils

Calculate what size an image needs to be to contain a file:

`bin/hide-inside util:min-size --file data/sample.txt`  
`bin/hide-inside util:min-size --size 8192`  

Use a specific aspect **r**atio:

`bin/hide-inside util:min-size --size 8192 -r 16:9`  

---

## Todo

- Write noise to unused pixels when hiding
- Better error handling
- Do something to support transparency (GD is terrible for that)
