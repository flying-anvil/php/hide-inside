<?php

declare(strict_types=1);

namespace FlyingAnvil\HideInside\Command;

use FlyingAnvil\HideInside\Service\RevealService;
use FlyingAnvil\Libfa\Wrapper\Image;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckForDataCommand extends Command
{
    public const COMMAND_NAME = 'check';

    private const ARGUMENT_INPUT  = 'input';

    public function __construct(
        private RevealService $revealService,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Reveal file from image');

        $this->addArgument(
            self::ARGUMENT_INPUT,
            InputArgument::REQUIRED,
            'Image to reveal data from',
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inputFile = $input->getArgument(self::ARGUMENT_INPUT);

        $inputImage = Image::loadFromFile($inputFile, 'png');
        $result = $this->revealService->checkIfContainsData(
            $inputImage,
        );

        if (!$result) {
            $output->writeln('Image does not contain data');
            return self::FAILURE;
        }

        $output->writeln('Image contains data');
        $header = $this->revealService->readHeader($inputImage);

        $this->revealService->printHeader($header);

        return self::SUCCESS;
    }
}
