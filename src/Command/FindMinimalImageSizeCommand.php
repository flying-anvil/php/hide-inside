<?php

declare(strict_types=1);

namespace FlyingAnvil\HideInside\Command;

use FlyingAnvil\HideInside\DataObject\HideHeader;
use FlyingAnvil\HideInside\Exception\HideInsideException;
use FlyingAnvil\HideInside\Service\HideService;
use FlyingAnvil\Libfa\DataObject\Math\Vector2;
use FlyingAnvil\Libfa\Wrapper\File;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FindMinimalImageSizeCommand extends Command
{
    public const COMMAND_NAME = 'util:min-size';

    private const OPTION_FILE  = 'file';
    private const OPTION_RATIO = 'ratio';
    private const OPTION_SIZE  = 'size';

    private Vector2 $ratio;
    private ?string $optionFile;
    private ?string $optionSize;
    private int $size;

    public function __construct(
        private HideService $hideService,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription(sprintf(
            'Figures out what minimal size the image has to be. Requires --%s or --%s',
            self::OPTION_FILE,
            self::OPTION_SIZE,
        ));

        $this->addOption(
            self::OPTION_FILE,
            'f',
            InputOption::VALUE_REQUIRED,
            'File to use for calculation',
        );

        $this->addOption(
            self::OPTION_SIZE,
            's',
            InputOption::VALUE_REQUIRED,
            'Size (in bytes) to use for calculation',
        );

        $this->addOption(
            self::OPTION_RATIO,
            'r',
            InputOption::VALUE_REQUIRED,
            'Force aspect ratio. Example: 16:9',
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->optionFile = $input->getOption(self::OPTION_FILE);
        $this->optionSize = $input->getOption(self::OPTION_SIZE);

        $rawRatio = $input->getOption(self::OPTION_RATIO);
        if ($rawRatio) {
            if (!preg_match('/(\d+):(\d+)/', $rawRatio, $matches)) {
                throw new HideInsideException('Malformed ratio: ' . $rawRatio);
            }

            $this->ratio = Vector2::create((int)$matches[1], (int)$matches[2]);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $validationResult = $this->validateOptions($output);
        if ($validationResult !== 0){
            return $validationResult;
        }

        $requiredBytes = $this->size + HideHeader::getHeaderSize();

        $sizeSquare = $this->findSize($requiredBytes, 1, 1);
        if ($sizeSquare) {
            $output->writeln(sprintf(
                'Min size square: %d×%d',
                $sizeSquare->getX(),
                $sizeSquare->getY(),
            ));
        }

        if (isset($this->ratio)) {
            $ratioSize = $this->findSize($requiredBytes, (int)$this->ratio->getX(), (int)$this->ratio->getY());
            if ($ratioSize) {
                $output->writeln(sprintf(
                    'Min size %d:%d: %d×%d',
                    $this->ratio->getX(),
                    $this->ratio->getY(),
                    $ratioSize->getX(),
                    $ratioSize->getY(),
                ));
            }
        }

        $accumulator = 16;
        for ($i = 0; $i < 17; $i++) {
            $usable = $this->hideService->getUsableBytes(
                $accumulator,
                $accumulator,
            );

            if ($usable >= $requiredBytes) {
                $output->writeln(sprintf(
                    'Min common size: %d×%d',
                    $accumulator,
                    $accumulator,
                ));
                break;
            }

            $accumulator = (int)($accumulator * ($i % 2 === 0
                ? 1.5
                : (4/3)
            ));
        }

        return self::FAILURE;
    }

    private function findSize(int $requiredBytes, int $ratioX, int $ratioY): ?Vector2
    {
        for ($i = 0; $i < 8192; $i++) {
            $usable = $this->hideService->getUsableBytes(
                $i * $ratioX,
                $i * $ratioY,
            );

            if ($usable >= $requiredBytes) {
                return Vector2::create(
                    $i * $ratioX,
                    $i * $ratioY,
                );
            }
        }

        return null;
    }

    private function validateOptions(OutputInterface $output): int
    {
        if ($this->optionFile === null && $this->optionSize === null) {
            $output->writeln(sprintf(
                'Must specify one of --%s or --%s',
                self::OPTION_FILE,
                self::OPTION_SIZE,
            ));

            return 2;
        }

        if ($this->optionFile !== null && $this->optionSize !== null) {
            $output->writeln(sprintf(
                'Options --%s and --%s are mutually exclusive',
                self::OPTION_FILE,
                self::OPTION_SIZE,
            ));

            return 2;
        }

        if ($this->optionFile !== null) {
            $file = File::load($this->optionFile);
            if (!$file->exists()) {
                $output->writeln(sprintf(
                    'File "%s" does not exist',
                    $this->optionFile,
                ));

                return 2;
            }

            $this->size = $file->getFileSize();
            return 0;
        }

        if ($this->optionSize !== null) {
            if (!ctype_digit($this->optionSize)) {
                $output->writeln(sprintf(
                    'Option --%s must contain only digits, "%s" given',
                    self::OPTION_SIZE,
                    $this->optionSize,
                ));

                return 2;
            }

            $this->size = (int)$this->optionSize;
        }

        return 0;
    }
}
