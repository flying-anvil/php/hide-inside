<?php

declare(strict_types=1);

namespace FlyingAnvil\HideInside\Command;

use FlyingAnvil\HideInside\Service\HideService;
use FlyingAnvil\Libfa\Wrapper\File;
use FlyingAnvil\Libfa\Wrapper\Image;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class HideCommand extends Command
{
    public const COMMAND_NAME = 'hide';

    private const ARGUMENT_DATA   = 'data';
    private const ARGUMENT_INPUT  = 'input';
    private const ARGUMENT_OUTPUT = 'output';

    private const OPTION_DEBUG    = 'debug';
    private const OPTION_NO_NOISE = 'no-noise';

    public function __construct(
        private HideService $hideService,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Hide file in image');

        $this->addArgument(
            self::ARGUMENT_DATA,
            InputArgument::REQUIRED,
            'The data to hide',
        );

        $this->addArgument(
            self::ARGUMENT_INPUT,
            InputArgument::REQUIRED,
            'Image to hide data in',
        );

        $this->addArgument(
            self::ARGUMENT_OUTPUT,
            InputArgument::REQUIRED,
            'Target to write the result',
        );

        $this->addOption(
            self::OPTION_NO_NOISE,
            'N',
            InputOption::VALUE_NONE,
            'Disables generating noise for unused pixels',
        );

        $this->addOption(
            self::OPTION_DEBUG,
            'd',
            InputOption::VALUE_NONE,
            'Turn on debugs coloring',
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $dataFile   = $input->getArgument(self::ARGUMENT_DATA);
        $inputFile  = $input->getArgument(self::ARGUMENT_INPUT);
        $outputFile = $input->getArgument(self::ARGUMENT_OUTPUT);

        if ($input->getOption(self::OPTION_NO_NOISE)) {
            $this->hideService->setNoise(false);
        }

        if ($input->getOption(self::OPTION_DEBUG)) {
            $this->hideService->setDebug(true);
        }

        $result = $this->hideService->hide(
            File::load($dataFile),
            Image::loadFromFile($inputFile, 'png'),
        );

        $result->write(filePath: $outputFile);

        return self::SUCCESS;
    }
}
