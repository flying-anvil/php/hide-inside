<?php

declare(strict_types=1);

namespace FlyingAnvil\HideInside\Command;

use FlyingAnvil\HideInside\Service\RevealService;
use FlyingAnvil\Libfa\Wrapper\File;
use FlyingAnvil\Libfa\Wrapper\Image;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RevealCommand extends Command
{
    public const COMMAND_NAME = 'reveal';

    private const ARGUMENT_INPUT  = 'input';
    private const ARGUMENT_OUTPUT = 'output';

    private const OPTION_NO_EXTENSION = 'no-extension';

    public function __construct(
        private RevealService $revealService,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Reveal file from image');

        $this->addArgument(
            self::ARGUMENT_INPUT,
            InputArgument::REQUIRED,
            'Image to reveal data from',
        );

        $this->addArgument(
            self::ARGUMENT_OUTPUT,
            InputArgument::OPTIONAL,
            'Target to write the result (extension is added automatically)',
        );

        $this->addOption(
            self::OPTION_NO_EXTENSION,
            'E',
            InputOption::VALUE_NONE,
            'Do not append the file extension to the output file',
        );

//        $this->addOption(
//            self::OPTION_DEBUG,
//            'd',
//            InputOption::VALUE_NONE,
//            'Turn on debugs coloring',
//        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inputFile  = $input->getArgument(self::ARGUMENT_INPUT);
        $outputFile = $input->getArgument(self::ARGUMENT_OUTPUT);

        if ($input->getOption(self::OPTION_NO_EXTENSION)) {
            $this->revealService->setNoExtension(true);
        }

        $result = $this->revealService->reveal(
            Image::loadFromFile($inputFile, 'png'),
            File::load($outputFile),
        );

        $output->writeln(sprintf(
            'Wrote result to "%s"',
            $result->getFilePath(),
        ));

        return self::SUCCESS;
    }
}
