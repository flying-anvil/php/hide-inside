<?php

declare(strict_types=1);

namespace FlyingAnvil\HideInside;

use DI\Container;
use DI\ContainerBuilder;
use FlyingAnvil\Libfa\DataObject\Application\AppEnv;

final class ContainerFactory
{
    public static function create(): Container
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions([
        ]);

        return $containerBuilder->build();
    }
}
