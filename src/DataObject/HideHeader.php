<?php

declare(strict_types=1);

namespace FlyingAnvil\HideInside\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Table;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class HideHeader implements DataObject
{
    public const MAGIC = 'HI!';
    public const VERSION = 0x01;

    private function __construct(
        private int $size,
        private string $fileExtension,
        private string $checksum,
        private int $version,
    ) {
        if ($size > (1 << 32) - 1) {
            throw new ValueException('Size is too big (max 32-bit)');
        }

        if (strlen($fileExtension) > 8) {
            throw new ValueException('File extension is too long (max 8 bytes)');
        }

        if (strlen($checksum) !== 4) {
            throw new ValueException('Checksum is malformed (must be 4 bytes)');
        }

        if ($this->version > 0xFF) {
            throw new ValueException('Version is too big (max 1 byte)');
        }
    }

    public static function create(
        int $size,
        string $fileExtension,
        string $checksum, // binary
        int $version = self::VERSION,
    ): self {
        return new self($size, $fileExtension, $checksum, $version);
    }

    public static function getHeaderSize(): int
    {
        return (
            strlen(self::MAGIC)
            + 1 // version
            + 4 // 32-bit size
            + 8 // 8 byte file extension
            + 4 // 32-bit checksum
        );
    }

    public function toBinaryString(): string
    {
        return (
            self::MAGIC
            . chr($this->version)
            . pack('N', $this->size)
            . str_pad($this->fileExtension, 8, "\0", STR_PAD_RIGHT)
            . $this->checksum
        );
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function getFileExtension(): string
    {
        return $this->fileExtension;
    }

    public function getChecksum(): string
    {
        return $this->checksum;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function jsonSerialize(): array
    {
        return [
            'size'          => $this->size,
            'fileExtension' => $this->fileExtension,
            'checksum'      => '0x' . bin2hex($this->checksum),
            'version'       => $this->version,
        ];
    }
}
