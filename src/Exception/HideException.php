<?php

declare(strict_types=1);

namespace FlyingAnvil\HideInside\Exception;

class HideException extends HideInsideException
{
}
