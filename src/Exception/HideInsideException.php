<?php

declare(strict_types=1);

namespace FlyingAnvil\HideInside\Exception;

use Exception;

class HideInsideException extends Exception
{
}
