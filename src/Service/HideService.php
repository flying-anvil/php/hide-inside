<?php

declare(strict_types=1);

namespace FlyingAnvil\HideInside\Service;

use FlyingAnvil\HideInside\DataObject\HideHeader;
use FlyingAnvil\HideInside\Exception\HideInsideException;
use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\Random\BuiltinRandomNumberGenerator;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use FlyingAnvil\Libfa\Utils\Progress\Options\ProgressBarOptions;
use FlyingAnvil\Libfa\Utils\Progress\ProgressBar;
use FlyingAnvil\Libfa\Wrapper\File;
use FlyingAnvil\Libfa\Wrapper\Image;
use Generator;

class HideService
{
    private bool $debug = false;
    private bool $noise = true;

    private const PACKET_SIZE = 6;

    public function hide(File $data, Image $inputImage): Image
    {
        $inputImage->open();
        $usableBytes   = $this->getUsableBytes($inputImage->getResolutionX(), $inputImage->getResolutionY());
        $requiredBytes = $data->getFileSize() + HideHeader::getHeaderSize();

        if ($usableBytes < $requiredBytes) {
            throw new HideInsideException(sprintf(
                '%d bytes required, but only %d are available',
                $requiredBytes,
                $usableBytes,
            ));
        }

        echo sprintf(
            'Using %d/%d bytes (%.2f%%)%s',
            $requiredBytes,
            $usableBytes,
            ($requiredBytes / $usableBytes) * 100,
            $this->noise ? ', padded with noise' : '',
        ), PHP_EOL;

        fwrite(STDERR, 'test');

        $expectedBytes = $this->noise ? $usableBytes : $requiredBytes;
        $totalPackets = (int)ceil(($expectedBytes) / (self::PACKET_SIZE/8));

        // TODO: Do it better with the ProgressBar once it's more flexible
        $consoleWriter = new ConsoleWriter(STDERR);
        $progressBar = new ProgressBar($consoleWriter);
        $progressBar->setOptions(ProgressBarOptions::createPresetSimpleBlocks());
        $progressBar->writeProgress(0, $totalPackets);

        $packetCount = 0;
        $output = $inputImage->copy();
        foreach ($this->iterateBitChunks($data, self::PACKET_SIZE, $usableBytes) as $index => $packet) {
            [$posX, $posY] = $output->pixelIndexToCoordinates((int)$index);
            $red   = $this->decBinPad(($packet & 0b00_11_00_00) >> 4, 2);
            $green = $this->decBinPad(($packet & 0b00_00_11_00) >> 2, 2);
            $blue  = $this->decBinPad(($packet & 0b00_00_00_11) >> 0, 2);

            $imageColor = $inputImage->getPixel($posX, $posY);

            $rawRed   = $this->decBinPad($imageColor->getRed(), 8);
            $rawGreen = $this->decBinPad($imageColor->getGreen(), 8);
            $rawBlue  = $this->decBinPad($imageColor->getBlue(), 8);

            // Cut away last 2 "bits" and append new "bits"
            $newRawRed   = substr($rawRed,   0, self::PACKET_SIZE) . $red;
            $newRawGreen = substr($rawGreen, 0, self::PACKET_SIZE) . $green;
            $newRawBlue  = substr($rawBlue,  0, self::PACKET_SIZE) . $blue;

//            if ($posX === 2 && $posY === 2) {
//                var_dump($rawRed);
//            }

            if ($this->debug) {
                // Make all "bits" the new ones
//                $newRawRed   = str_repeat($red, 4);
//                $newRawGreen = str_repeat($green, 4);
//                $newRawBlue  = str_repeat($blue, 4);

                // Make pixel new "bits" the only ones
                $newRawRed   = $red;
                $newRawGreen = $green;
                $newRawBlue  = $blue;
            }

            $newRed   = bindec($newRawRed);
            $newGreen = bindec($newRawGreen);
            $newBlue  = bindec($newRawBlue);

//            echo sprintf('Red:   %s + %s => %s', $rawRed, $red, $newRawRed), PHP_EOL;
//            echo sprintf('Green: %s + %s => %s', $rawGreen, $green, $newRawGreen), PHP_EOL;
//            echo sprintf('Blue:  %s + %s => %s', $requiredBytes, $blue, $newRawBlue), PHP_EOL;
//
//            die;

            $output->setPixel($posX, $posY, Color::create(
                $newRed,
                $newGreen,
                $newBlue,
            ));

            // TODO: don't write progress each byte
            $progressBar->writeProgress(++$packetCount, $totalPackets);

//            usleep(1000);
        }

        $consoleWriter->ansiClearLine();
        echo "\e[9001DDone", PHP_EOL;

        return $output;
    }

    public function getUsableBytes(int $resX, int $resY): int
    {
        $pixels = $resX * $resY;
        $bits   = $pixels * 24; // Without alpha
        $usable = $bits * (2 / 8); // Using bytes 00000011

        return (int)floor($usable / 8);
    }

    public function setDebug(bool $value = true): void
    {
        $this->debug = $value;
    }

    public function setNoise(bool $noise = true): void
    {
        $this->noise = $noise;
    }

    /**
     * @return Generator | Array<int>
     */
    private function iterateBitChunks(File $file, int $chunkSize, int $usableBytes): Generator
    {
        $header = HideHeader::create(
            $file->getFileSize(),
            $file->getExtension()->toString(),
            hash_file('crc32', $file->getFilePath(), true),
        );

        $packet = 0;
        $packetSize = 0;
        foreach ($this->iterateDataBytes($header, $file, $usableBytes) as $char) {
            $bin = decbin(ord($char));
            $bin = str_pad($bin, 8, '0', STR_PAD_LEFT);

            foreach (str_split($bin) as $bit) {
                $packetSize++;
//                echo sprintf(
//                    'PSize: %d | Packet: %s | Adding: %s | Result: %s',
//                    $packetSize,
//                    str_pad(decbin($packet), 6, '0', STR_PAD_LEFT),
//                    str_pad(decbin((int)($bit) << ($chunkSize - $packetSize)), 6, '0', STR_PAD_LEFT),
//                    str_pad(decbin($packet + ((int)($bit) << ($chunkSize - $packetSize))), 6, '0', STR_PAD_LEFT),
//                ), PHP_EOL;

                $packet += (int)($bit) << ($chunkSize - $packetSize);
                if ($packetSize === $chunkSize) {
                    yield $packet;
                    $packet = 0;
                    $packetSize = 0;
                }
            }
        }

        // yield the rest
        if ($packetSize > 0) {
            yield $packet;
        }
    }

    private function iterateDataBytes(HideHeader $header, File $data, int $usableBytes): Generator
    {
        $index = 0;

        $headerBinary = $header->toBinaryString();
        foreach (str_split($headerBinary) as $char) {
            yield $index++ => $char;
        }

        if (!$data->isOpen()) {
            $data->open();
        }

        foreach ($data->iterateChars() as $char) {
            yield $index++ => $char;
        }

        if ($this->noise) {
            $usedBytes   = HideHeader::getHeaderSize() + $data->getFileSize();
            $unusedBytes = $usableBytes - $usedBytes;

            $rng = new BuiltinRandomNumberGenerator();

            for ($i = 0; $i < $unusedBytes; $i++) {
                yield $index++ => chr($rng->generateRangeInt(0x00, 0xFF));
            }
        }
    }

    private function decBinPad(int $bin, int $length = 6): string
    {
        return str_pad(decbin($bin), $length, '0', STR_PAD_LEFT);
    }
}
