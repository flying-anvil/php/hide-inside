<?php

declare(strict_types=1);

namespace FlyingAnvil\HideInside\Service;

use FlyingAnvil\HideInside\DataObject\HideHeader;
use FlyingAnvil\HideInside\Exception\RevealException;
use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Table;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutput;
use FlyingAnvil\Libfa\Utils\Formatter\KeyValueFormatter;
use FlyingAnvil\Libfa\Utils\Progress\Options\ProgressBarOptions;
use FlyingAnvil\Libfa\Utils\Progress\ProgressBar;
use FlyingAnvil\Libfa\Wrapper\File;
use FlyingAnvil\Libfa\Wrapper\Image;
use Generator;

class RevealService
{
    private const PACKET_SIZE = 6;

    private bool $noExtension = false;

    public function reveal(Image $inputImage, File $outputFile): File
    {
        $inputImage->openIfClosed();

        $header = $this->readHeader($inputImage);

        if ($header->getVersion() !== 1) {
            throw new RevealException(sprintf(
                'Unsupported Version "%s"',
                $header->getVersion(),
            ));
        }

        echo sprintf(
            'Reading %d bytes',
            $header->getSize(),
        ), PHP_EOL;

        $headerSize = HideHeader::getHeaderSize();
        $totalBytes = $header->getSize();
        $generator = $this->iterateExtractedBytes(
            $inputImage,
            $headerSize + $totalBytes,
        );

        // TODO: Do it better with the ProgressBar once it's more flexible
        $consoleWriter = new ConsoleWriter(STDERR);
        $progressBar = new ProgressBar($consoleWriter);
        $progressBar->setOptions(ProgressBarOptions::createPresetSimpleBlocks());
        $progressBar->writeProgress(0, $totalBytes);

        $writtenBytes = 0;

        $outputWithExtension = $this->noExtension
            ? $outputFile->getFilePath()
            : sprintf(
            '%s.%s',
            $outputFile->getFilePath(),
            $header->getFileExtension(),
        );

        $output = File::load($outputWithExtension);
        $output->open('wb');
        foreach ($generator as $index => $byte) {
            // Skip header
            if ($index < $headerSize) {
                continue;
            }

            $output->write(chr($byte));
            $writtenBytes++;

            // TODO: don't write progress each byte
            $progressBar->writeProgress($writtenBytes, $totalBytes);
        }

        $consoleWriter->ansiClearLine();
        echo PHP_EOL;

        $output->close();

        if (!$this->verify($header, $output)) {
            echo sprintf(
                '%s: Checksum of header and file do not match',
                DecorableOutput::create('Warning')->setForegroundColor(Color::create(225, 5, 25))->setUnderline(),
            ), PHP_EOL;
        }

        return $output;
    }

    public function readHeader(Image $input): HideHeader
    {
        $input->openIfClosed();
        $rawHeader = iterator_to_array($this->iterateExtractedBytes(
            $input,
            HideHeader::getHeaderSize(),
        ));

        $magic         = $this->concatByteString($rawHeader, 0, 3);
        $version       = $rawHeader[3];
        $size          = unpack('N', $this->concatByteString($rawHeader, 4, 4))[1];
        $fileExtension = rtrim($this->concatByteString($rawHeader, 8, 8), "\0");
        $checksum      = $this->concatByteString($rawHeader, 16, 4);

        if ($magic !== HideHeader::MAGIC) {
            throw new RevealException('Magic mismatch, input does not contain revealable data');
        }

        return HideHeader::create(
            $size,
            $fileExtension,
            $checksum,
            $version,
        );
    }

    public function checkIfContainsData(Image $input): bool
    {
        $input->openIfClosed();
        $rawHeader = iterator_to_array($this->iterateExtractedBytes(
            $input,
            3,
        ));

        $magic = $this->concatByteString($rawHeader, 0, 3);
        return $magic === HideHeader::MAGIC;
    }

    public function setNoExtension(bool $noExtension): void
    {
        $this->noExtension = $noExtension;
    }

    private function iterateExtractedBytes(Image $image, int $amount): Generator
    {
        $pixelCount = (int)ceil($amount * (8 / self::PACKET_SIZE));

        $byteSize = 0;
        $byte     = 0;
        for ($i = 0; $i < $pixelCount; $i++) {
            [$posX, $posY] = $image->pixelIndexToCoordinates($i);
            $color = $image->getPixel($posX, $posY);

            $partRed = $color->getRed() & 0b00_00_00_11;
            $byte = ($byte << 2) + $partRed;
            $byteSize += 2;
            if ($byteSize === 8) {
                yield $byte;
                $byteSize = 0;
                $byte     = 0;
            }

            $partGreen = $color->getGreen() & 0b00_00_00_11;
            $byte = ($byte << 2) + $partGreen;
            $byteSize += 2;
            if ($byteSize === 8) {
                yield $byte;
                $byteSize = 0;
                $byte     = 0;
            }

            $partBlue = $color->getBlue() & 0b00_00_00_11;
            $byte = ($byte << 2) + $partBlue;
            $byteSize += 2;
            if ($byteSize === 8) {
                yield $byte;
                $byteSize = 0;
                $byte     = 0;
            }
        }
    }

    private function concatByteString(array $data, int $start, int $length): string
    {
        $result = '';
        for ($i = $start; $i < $start + $length; $i++) {
            $result .= chr($data[$i]);
        }

        return $result;
    }

    public function printHeader(HideHeader $header): void
    {
        $keyValueFormatter = new KeyValueFormatter();
        $formatted = $keyValueFormatter->formatKeyValueTable($header->jsonSerialize());
        echo $formatted, PHP_EOL;
    }

    private function verify(HideHeader $header, File $output): bool
    {
        $fileChecksum = hash_file('crc32', $output->getFilePath(), true);

        return $fileChecksum === $header->getChecksum();
    }
}
